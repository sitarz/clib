#include <stdio.h>
#include "vector.h"

int main( void )
{
    const uint32_t len = 5;

    Vector* v = Vector_Alloc( len, sizeof( int ) );

    uint32_t i;

    for ( i = 0; i < len; ++i )
    {
        Vector_PushBack( v, ( element_t )i );
    }

    for ( i = 0; i < len; ++i )
    {
        printf( "%i\n", ( int ) Vector_Get( v, i ) );
    }

    Vector_Free( v );

    return 0;
}


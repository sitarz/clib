#pragma once

#include <stdint-gcc.h>
#include <stdio.h>
#include <stdlib.h>

struct defVector;

typedef struct defVector Vector;

typedef void* element_t;

Vector*     Vector_Alloc( uint32_t len, size_t elemSize );

void        Vector_PushBack( Vector* const v, element_t element );

void        Vector_PopBack( Vector* const v );

element_t   Vector_Get( Vector* const v, uint32_t i );

void        Vector_Free( Vector* v );

TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt


QMAKE_CXXFLAGS += -Wall -Werror -pedantic -std=gnu99

SOURCES += main.c \
    vector.c

HEADERS += \
    vector.h


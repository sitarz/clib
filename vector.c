#include "vector.h"
#include <assert.h>

typedef struct defVector
{
    uint32_t        length;

    element_t       buffer;

    size_t          size;

    size_t          elemSize;
}
Vector;

static void __ReallocVector( Vector* const v, uint32_t len )
{
    v->size     = v->elemSize * len;
    v->length   = len;

    element_t newBuffer = realloc( v->buffer, v->size );

    assert( newBuffer != NULL );

    v->buffer = newBuffer;
    //newBuffer = NULL;
}

Vector*     Vector_Alloc( uint32_t len, size_t elemSize )
{
    Vector* v = ( Vector* ) malloc( sizeof( Vector ) );

    assert( v != NULL );

    v->size   = len * elemSize;

    v->elemSize = elemSize;

    v->buffer = malloc( v->size );

    assert( v->buffer != NULL );

    v->length = 0;

    return v;
}

void        Vector_PushBack( Vector * const v, element_t element )
{
    __ReallocVector( v, v->length + 1 );

    //! v->length takes on value passed to __ReallocVector's second param

    *( v->buffer + v->length ) = element;
}

element_t  Vector_Get( Vector * const v, uint32_t i )
{
    assert( i < v->length );

    return *( v->buffer + i );
}

void        Vector_Free( Vector* v )
{
    if ( v )
    {
        if ( v->buffer )
        {
            free( v->buffer );
        }

        free( v );
    }
}

